var gulp = require('gulp'),

    concat = require('gulp-concat'),
    livereload = require('gulp-livereload'),
    less = require('gulp-less'),
    mainBowerFiles = require('main-bower-files'),
    streamqueue = require('streamqueue'),
    templateCache = require('gulp-angular-templatecache'),

    server = require('./server'),

    distPath = './dist/',
    sources = {
      js: './app/**/!(service-worker).js',
      less: './app/**/*.less',
      index: './app/index.html',
      fonts: './bower_components/bootstrap/fonts/*.+(woff|woff2)',
      worker: ['./app/service-worker.js'],
      templates: './app/**/*.html'
    };

gulp.task('index', function () {
  return gulp.src(sources.index)
    .pipe(gulp.dest(distPath))
    .pipe(livereload());
});

gulp.task('worker', function () {
  return gulp.src(sources.worker)
    .pipe(gulp.dest(distPath));
});

gulp.task('js:app', function () {
  return streamqueue({ objectMode: true },
    gulp.src(sources.js),
    gulp.src(sources.templates)
      .pipe(templateCache({module: 'truthNews'})))
    .pipe(concat('app.js'))
    .pipe(gulp.dest(distPath))
    .pipe(livereload());
});

gulp.task('js:vendor', function () {
  return gulp.src(mainBowerFiles('**/!(jquery|bootstrap).js'))
    .pipe(concat('vendor.js'))
    .pipe(gulp.dest(distPath))
    .pipe(livereload());
});

gulp.task('less', function () {
  return gulp.src('./app/app.less')
    .pipe(less())
    .pipe(gulp.dest(distPath))
    .pipe(livereload());
});

gulp.task('fonts', function () {
  return gulp.src(sources.fonts)
    .pipe(gulp.dest(distPath + 'fonts'));
});

gulp.task('default', ['index', 'worker', 'fonts', 'js:app', 'js:vendor', 'less'], function (done) {
  livereload.listen();
  gulp.watch(sources.js, ['js:app']);
  gulp.watch(sources.templates, ['js:app']);
  gulp.watch(sources.less, ['less']);
  gulp.watch(sources.index, ['index']);
  gulp.watch(sources.worker, ['worker']);
  server(done);
});
