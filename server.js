module.exports = function (done) {
  var connectLivereload = require('connect-livereload'),
      express = require('express'),

      setupAPI = require('./api'),

      app = express();

  app.use(connectLivereload());

  app.use(function (req, res, next) {
    setTimeout(function () { next(); }, 500);
  });

  app.use(express.static('./dist'));

  app.get('/news/*', function (req, res) {
    return res.sendFile(__dirname + '/dist/index.html');
  });

  setupAPI(app);

  var server = app.listen(7777, function () {
    console.log('Express server listening at http://localhost:%s', server.address().port);
    done();
  });
};
